# Grafana Exploration Survival Guide

1. [VM Grafana](#vm-grafana)
   - 1.1 CPU
   - 1.2 Memoria RAM
   - 1.3 Almacenamiento

2. [Instance configuration - Install Grafana](#instance-configuration---install-grafana)
   - 2.1 EC2: Install Grafana
   - 2.2 EC2: RUN Grafana

3. [Prepar AWS Account](#prepar-aws-account)
   - 3.1 AWS: Create custom policy
   - 3.2 AWS: Create user "grafana"

4. [Grafana configuration](#grafana-configuration)
   - 4.1 Grafana: Datasource configuration (AWS)
     - 4.1.1 Create X-Ray datasource connection
     - 4.1.2 Create X-Ray Cloudwatch connection

5. [Configuration panel dashboard](#configuration-panel-dashboard)
   - 5.1 Dashboard component
   - 5.2 Graph component

6. [Import Graph - EC2](#import-graph---ec2)

7. [Alert definition](#alert-definition)


# **(1) VM Grafana**

- CPU: Se recomienda contar con al menos 2 núcleos de CPU para manejar el procesamiento de las consultas y la generación de gráficos de forma eficiente. Si esperas un alto volumen de consultas concurrentes o tienes paneles muy complejos, es posible que necesites más núcleos de CPU.

- Memoria RAM: La cantidad de memoria RAM necesaria dependerá de varios factores, como el tamaño de los conjuntos de datos, la cantidad de paneles abiertos simultáneamente y la carga de trabajo esperada. Como referencia, se recomienda tener al menos 4 GB de RAM para un entorno básico, pero es posible que necesites más memoria si manejas grandes volúmenes de datos o si tienes paneles complejos.

- Almacenamiento: El espacio de almacenamiento necesario dependerá del tamaño de la instalación de Grafana, la cantidad de datos históricos que planeas almacenar y cualquier otro archivo adicional que desees respaldar, como snapshots de paneles. 




---

# **(2) Instance configuration - Install grafana**

Antes de tomar la desición de instalar Grafana o un servicio de visualización de datos, considera:

1) **Escala y alcance:** Evalúa la escala de tu organización y el alcance del proyecto. Si tienes múltiples proyectos independientes con diferentes equipos, es posible que desees un despliegue por proyecto. Si tu organización es grande y centralizada, un despliegue organizativo puede ser más adecuado.

2) **Requisitos de seguridad:** Analiza los requisitos de seguridad de tu organización. Si necesitas un control más estricto sobre los datos y el acceso, un despliegue organizativo te brinda mayor control centralizado. Si los proyectos tienen requisitos de seguridad diferentes, un despliegue por proyecto puede permitir una mayor personalización.

3) **Gestión y mantenimiento:** Considera los recursos disponibles para gestionar y mantener Grafana. Un despliegue organizativo puede requerir más esfuerzo de administración centralizada, mientras que un despliegue por proyecto puede delegar parte de la responsabilidad de gestión a los equipos individuales.

4) **Integración y colaboración:** Evalúa la necesidad de compartir paneles y datos entre proyectos. Si la colaboración y la integración son fundamentales, un despliegue organizativo puede facilitar la conexión y el intercambio de información.

## 2.1 - EC2: Install grafana

> Utilice la documentación oficial de Grafana:https://grafana.com/docs/grafana/latest/setup-grafana/installation/debian/

## 2.2 - EC2: RUN Grafana

> Utilice la documentación oficial de Grafana:  https://grafana.com/docs/grafana/latest/setup-grafana/start-restart-grafana/

---

# **(3) Prepair AWS Account**
Tenga en consideración la zona sobre la cual cree los recursos necesarios por Grafana, será necesario mas adelante.

## 3.1 - AWS: Create custom policy
Policy name: **AmazonGrafanaCloudWatchPolicy-01**

<details>
<summary>Policy JSON File</summary>

```
{
    "Version": "2012-10-17",
    "Statement": [
      {
        "Sid": "AllowReadingMetricsFromCloudWatch",
        "Effect": "Allow",
        "Action": [
          "cloudwatch:DescribeAlarmsForMetric",
          "cloudwatch:DescribeAlarmHistory",
          "cloudwatch:DescribeAlarms",
          "cloudwatch:ListMetrics",
          "cloudwatch:GetMetricData",
          "cloudwatch:GetInsightRuleReport"
        ],
        "Resource": "*"
      },
      {
        "Sid": "AllowReadingLogsFromCloudWatch",
        "Effect": "Allow",
        "Action": [
          "logs:DescribeLogGroups",
          "logs:GetLogGroupFields",
          "logs:StartQuery",
          "logs:StopQuery",
          "logs:GetQueryResults",
          "logs:GetLogEvents"
        ],
        "Resource": "*"
      },
      {
        "Sid": "AllowReadingTagsInstancesRegionsFromEC2",
        "Effect": "Allow",
        "Action": ["ec2:DescribeTags", "ec2:DescribeInstances", "ec2:DescribeRegions"],
        "Resource": "*"
      },
      {
        "Sid": "AllowReadingResourcesForTags",
        "Effect": "Allow",
        "Action": "tag:GetResources",
        "Resource": "*"
      }
    ]
}
````

</details>

## 3.2 - AWS: Create user "grafana"
> User: grafana
> Policy atach: \
    - AWSXrayFullAccess \
    - AmazonGrafanaCloudWatchPolicy-01 \
> Generate accessKey and secretKey

> **NOTA:** como sugerencia, al utilizar estructuras de multiples proyectos o cuentas cloud (AWS), se sugiere asociar un identificador al usuario "grafana-[ACCOUNT_NAME]"

# **(4) Grafana configuration**

## 4.1 - Grafana: Datasource configuration (AWS)

Integrar AWS CloudWatch y AWS X-Ray con Grafana para el monitoreo en AWS brinda una visión completa de la salud y el rendimiento de tu infraestructura. 

CloudWatch recopila métricas y registros de servicios de AWS, mientras que X-Ray realiza el seguimiento de aplicaciones distribuidas. Al utilizar Grafana como herramienta de visualización, puedes crear paneles personalizados y comprensibles que te ayuden a identificar problemas y tendencias en tiempo real. Esto permite un análisis profundo y un diagnóstico rápido de rendimiento, facilitando la resolución de problemas y la optimización continua. La integración de estas herramientas proporciona un monitoreo eficaz y una mejor comprensión de tu entorno en la nube.

### 4.1.1- **Create X-Ray datasource connection**

**I)** Connections > Connect data> X-Ray \
**II)** Click on "Create X-Ray Data Connection"
> Add accessKey \
> Add secretKey \
> Add Region resources.

### 4.2.2- **Create X-Ray Cloudwatch connection**

**I)** Connections > Connect data> Cloudwatch \
**II)** Click on "Create Cloudwatch Data Connection"
> Add accessKey \
> Add secretKey \
> Add Region resources \
> Select X-Ray [Step 5.2.1]

# **(5) Configuration panel dashboard**


Los dashboard de grafana pueden ser configurados desde 0 aprovicionando los origenes de datos y varaibles adecuados; con estas estructuras podras elaborar tus nuevos graficos.

Otra alternativa, es adoptar modelos prefabricados por los proveedores cloud o bien dispuestos por la misma comunidad.

Es posible importar "Dashbards" desde los repositorios de grafana:
> https://grafana.com/grafana/dashboards/

Los paneles prefabricados cuentan con configuración pre establecidas que es posible customizar a nuestras necesidades.

Los paneles importados ya tienen una serie de variables definidas las cuales pueden ser utilizadas como referencias, modificar y/o crear nuevas valores con el objetivo de añadir nuevas caracteristicas a los paneles.

Sigue estos pasos para crear tus propios dashboard:

1. Definir los objetivos del dashboard
2. Identificar las fuentes de datos
3. Configurar las conexiones de datos
4. Diseñar paneles y gráficos (Se sugiere leer hacer de las estrategias de visualización de datos)
5. Aplicar filtros y consultas
6. Configurar alertas
7. Realizar pruebas y ajustes.


## 5.1 - Dashboard component
- Datasource
    > El datasource disponibiliza una serie de metricas que podras ser utilizadas en la elboración de graficos.
    Cada origen de dato (datasource) posee metricas que puedes ser exploradas mediante el "explorador" de grafana
- Variables
    > Las variables permiten filtrar y personalizar los parametros de de nuestras visualizaciónes basandose en los parametros estructurales.
    estas variables  puede ser de tipo: 
    > - Query (para filtrar)
    > - Custom
    > - Constant
    > - Datasource
    > - Interval.


## 5.2 - Graph component
- Visualization graph
> Tipo de grafico que podemos utilizar, estas estructuras pueden ser una estandar como "Lineas de tiempo", "Graficos de barra", "Heat map"", etc. A partir de las versiones 7.0 de grafana es posible utilizar versiones sugeridas por la plataforma.


# **(6) - Import Graph - EC2**

Dashboard > "new" > "import"

(Opcion 1) Importar JSON con las configuraciones.
(Opcion 2) Importar directamentes desde los repositorios de grafana mediante UID

# **(7) - Alert definition**

1. Accede al panel de Grafana donde deseas configurar la alerta.

2. Selecciona el gráfico específico al que deseas agregar la alerta o crea un nuevo gráfico si aún no lo tienes.

3. Haz clic en el icono de "Panel Inspector" en la esquina superior derecha del panel para abrir el inspector.

4. En el inspector, selecciona la pestaña "Metrics" para ver las métricas y las consultas asociadas al gráfico.

5. Identifica la métrica o consulta en la que deseas basar la alerta. Puede ser una consulta SQL, una función matemática, una métrica de una fuente de datos, etc.

6. Define los umbrales de la alerta. Esto implica establecer los límites superiores e inferiores que desencadenarán la alerta cuando se alcancen o se superen.

7. Decide la frecuencia y la duración que deben cumplir los valores de la métrica para que se active la alerta. Por ejemplo, puedes configurar que la métrica supere el umbral durante al menos 5 minutos antes de que se active la alerta.

8. Configura las acciones de la alerta. Puedes elegir qué acciones tomar cuando se active la alerta, como enviar notificaciones por correo electrónico, mensajes a través de Slack, generar registros de eventos, etc.

9. Guarda la configuración de la alerta y aplica los cambios en el panel de Grafana.

10. Realiza pruebas para asegurarte de que la alerta se active correctamente cuando se cumplan las condiciones establecidas. Ajusta los umbrales y las condiciones según sea necesario.
